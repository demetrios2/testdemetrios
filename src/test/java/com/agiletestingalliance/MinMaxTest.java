package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {
    @Test
    public void frought() {
        MinMax minMax = new MinMax();
        assertEquals(5, minMax.frought(3, 5));
        assertEquals(10, minMax.frought(10, 7));
        assertEquals(-1, minMax.frought(-1, -5));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("hello", minMax.bar("hello"));
        assertEquals("", minMax.bar(""));
        assertEquals("", minMax.bar(null));
    }
}
